#!/usr/bin/python
# -*- coding: utf-8 -*-
# MIT License, Copyright (c) 2022, Damien Andre

import matplotlib.pyplot as plt

it       = []
force    = []
position = []

f = open('data.txt', 'r')
for line in f:
    if not '#' in line:
        data = line.split()
        it.append(int(data[0]))
        force.append(float(data[1]))
        position.append(float(data[2]))
f.close()
plt.figure()
plt.plot(position, force, 'o-')
plt.xlabel("position (m)")
plt.ylabel("force (N)")
plt.show()

