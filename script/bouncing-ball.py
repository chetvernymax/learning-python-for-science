#!/usr/bin/python
# -*- coding: utf-8 -*-
# MIT License, Copyright (c) 2022, Damien Andre

import numpy as np
import matplotlib.pyplot as plt


import matplotlib
font = {'family' : 'normal',
        'weight' : 'bold',
        'size'   : 30}
matplotlib.rc('font', **font)


# read data file 
t, p_x, p_y = np.loadtxt("./bouncing-ball.txt", delimiter='\t', usecols=(0, 1, 2), unpack=True)

# plot 
plt.figure()
plt.plot(t, p_x, lw=3, label="position on x")
plt.plot(t, p_y, lw=3, label="position on x")
plt.xlabel("time (s)")
plt.ylabel("position (m)")
plt.legend()


# plot the ball's trajectory
plt.figure()
plt.plot(p_x, p_y, "o")
plt.xlim(0,500)
plt.ylim(0,100)
plt.gca().set_aspect('equal', adjustable='box')
plt.xlabel("x position (m)")
plt.ylabel("y position (m)")
plt.title("bouncing ball's trajectory")

# compute velocities thanks to np.gradient
dt  = 0.004
v_x = np.gradient(p_x, dt) 
v_y = np.gradient(p_y, dt)

# plot velocities with arrows
s = 0.08 # a factor to get good arrow lengths
for i in range(len(p_x)):
    p_xi, p_yi = p_x[i], p_y[i]
    v_xi, v_yi = v_x[i], v_y[i]
    plt.arrow(p_xi, p_yi, v_xi*s, v_yi*s, width=.8) 


# simulation parameters 
g    = -9.81
p_x0 = 10.
p_y0 = 80.
v_x0 = 10.
v_y0 = 0.

# equation of motion
p_xa = v_x0 * t + p_x0
p_ya = 0.5*g*np.power(t,2) + p_y0

# plot the ball's trajectory and compare
# with analytical formula 
plt.figure()
plt.plot(p_x, p_y  , "o" , label="simulation data")
plt.plot(p_xa, p_ya, "--", label="analytics" )
plt.xlim(0,50)
plt.ylim(0,100)
plt.xlabel("x position (m)")
plt.ylabel("y position (m)")
plt.title("bouncing ball's trajectory")
plt.legend()


# show all the matplotlib figures
plt.show()
