#!/usr/bin/python
# -*- coding: utf-8 -*-
# MIT License, Copyright (c) 2018, Damien Andre, Alexandre Boulle

import numpy as np
import matplotlib.pyplot as plt


# generate an array of graduation  
notes = np.random.normal(12, 2, 500)


print("rating number                  ;", notes.size)
print("min value                      :", notes.min())
print("max value                      :", notes.max())
print("mean value                     :", notes.mean())
print("standard dev                   :", notes.std())
print("cumulative sum                 :", notes.sum())
print("number of values higher than 15:", notes[notes > 15].size)

notes.sort()
print("sorted rating ",notes)

# plot histogram
plt.figure()
plt.hist(notes, 10)
plt.xlim(0,20)
plt.show()
