#!/usr/bin/python
# -*- coding: utf-8 -*-
# MIT License, Copyright (c) 2018, Damien Andre

import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import numpy as np


# read image in grascale 16 bits (uint16)
img = mpimg.imread('grains.tif')

# crop image by removing the info bar at the bottom
img = img[0:1000, :]

# display image in grayscale
plt.figure()
plt.imshow(img, cmap=plt.get_cmap('gray'))

# now, we will work on a copy of image
img1 = img.copy()

# we apply grayscale treshold 
img1[img1<25000] = 0
img1[img1>30000] = 2**16-1

# display the treshold image
plt.figure()
plt.imshow(img1, cmap=plt.get_cmap('gray'))

# let's apply a gradient
grad_x, grad_y = np.gradient(img1)

# takes the average of d(image)/dx and d(image)/dy
img2 = (grad_x + grad_y)/2

# apply a treshold
img2[img2<1000] = 2**16-1

# and display it
plt.figure()
plt.imshow(img2, cmap=plt.get_cmap('gray'))

# that's all
plt.show()
