#!/usr/bin/python
# -*- coding: utf-8 -*-
# MIT License, Copyright (c) 2018, Damien Andre, Alexandre Boulle

import numpy as np
import matplotlib.pyplot as plt
from math import sin


x = np.arange(0, 1e7, 1)


def sin_py(x):
    # built a numpy list with equal length
    res = [0 for i in range(len(x))]
    # compute sin for each element in sinus
    for i_x, v_x in enumerate(x):
        res[i_x] = sin(v_x)
    return res


import time
t0 = time.time()
sin_py(x)
t1 = time.time()
print ('elapsed time for sin_py(x) is',t1-t0, 's') 

t0 = time.time()
np.sin(x)
t1 = time.time()
print ('elapsed time for np.sin(x) is ',t1-t0, 's') 
