#!/usr/bin/python
# -*- coding: utf-8 -*-
# MIT License, Copyright (c) 2018, Damien Andre

import matplotlib.pylab as plt
import numpy as np

import matplotlib
font = {'family' : 'normal',
        'weight' : 'bold',
        'size'   : 30}
matplotlib.rc('font', **font)

x = np.linspace(0, 4*np.pi, 100.)
y = np.cos(x)
plt.plot(x,y)
plt.show()
