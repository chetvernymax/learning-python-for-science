#!/usr/bin/python
# -*- coding: utf-8 -*-
# MIT License, Copyright (c) 2018, Damien Andre


import glob
# note for me : in emacs 'c-c c-c' to switch in image mode

dic = {}

for filename in glob.glob('img/*.tif'):
    print("reading", filename, "...")    
    with open(filename, 'r', encoding='ascii',errors='ignore') as f:
        t = None
        p = None
        for line in f:
            if 'Temperature' in line:
                t = float(line.replace('Temperature=', ''))
            if 'Pressure' in line:
                p = float(line.replace('ChPressure=', ''))
        print ('temperature = {} °C\npressure    = {} Pa'.format(t,p))
        dic[filename] = {}
        dic[filename]['temperature'] = t
        dic[filename]['pressure']    = p

# now, write the file csv file
with open('img.db', 'w') as f:
    f.write("FileName \t Temperature \t Pressure\n")
    for filename in dic:
        temp  = dic[filename]['temperature']
        press = dic[filename]['pressure']
        f.write("{} \t {} \t {}\n".format(filename, temp, press))


# now write file in json format
import json
with open('img.json', 'w') as jfile:
    json.dump(dic, jfile, indent=2)
