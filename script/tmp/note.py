import scipy.io.wavfile
import numpy as np
import matplotlib.pyplot as plt

rate, signal = scipy.io.wavfile.read("wav-file/440Hz.wav")

if signal.ndim is 2:
    signal = signal[:,0]
    
t = np.arange(0., signal.size/rate, 1./rate) 
plt.figure()
plt.plot(t, signal)

spec = np.abs(np.fft.fft(signal))
freq = np.fft.fftfreq(signal.size, 1./rate)

spec = spec[0:spec.size/2]
freq = freq[0:freq.size/2]

plt.plot(freq, spec)
plt.xlabel('freq (Hz)')
plt.ylabel('abs(fft)')

plt.show()


