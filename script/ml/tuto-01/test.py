import tensorflow as tf
from tensorflow import keras
import numpy as np
import sys
import matplotlib.pyplot as plt
import cv2
print("Python     : {}".format(sys.version.split()[0]))
print("tensorflow : {} incluant keras {}".format(tf.__version__, keras.__version__))
print("numpy      : {}".format(np.__version__))
print("cv2        : {}".format(cv2.__version__))

