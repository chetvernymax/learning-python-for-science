import tensorflow as tf
from tensorflow import keras
import numpy as np
import sys
import matplotlib.pyplot as plt
import cv2
from tensorflow.keras import layers
import mltool

(im_train, lab_train), (im_test , lab_test) = keras.datasets.mnist.load_data()

## normalizing images
im_train = im_train / 255.
im_test  = im_test  / 255.

## vectorized images
x_train = im_train.reshape((60000, 784))
x_test  = im_test.reshape((10000 , 784))

## conversion in one-hot vector of lab_train
y_train = tf.keras.utils.to_categorical(lab_train, num_classes=10)
y_train = tf.constant(y_train, shape=[60000, 10])

## conversion in one-hot vector of lab_test
y_test = tf.keras.utils.to_categorical(lab_test, num_classes=10)
y_test = tf.constant(y_test, shape=[10000, 10])

## building the neural network
model = keras.Sequential()
model.add(keras.Input(shape=(784,)))
model.add(layers.Dense(784, activation="relu"))
model.add(layers.Dense(10, activation="softmax"))
model.compile(loss='categorical_crossentropy', optimizer="adam", metrics=["accuracy"])

## training the network with the test data 
hist = model.fit(x_train, y_train, validation_data=(x_test, y_test), batch_size=128, epochs=15)

## plot the loss and the accuracy 
mltool.plot_loss_accuracy(hist)

