import tensorflow as tf
from tensorflow import keras
import numpy as np
import sys
import matplotlib.pyplot as plt
import cv2

(im_train, lab_train), (im_test , lab_test) = keras.datasets.mnist.load_data()

##########
# PART 1 #
##########

## normalizing images
im_train = im_train / 255.
im_test  = im_test  / 255.

## size of the vectorized image 
print("the size of vectorized image should be:", 28*28)

## vectorized images
x_train = im_train.reshape((60000, 784))
x_test  = im_test.reshape((10000 , 784))

##########
# PART 2 #
##########

## conversion in one-hot vector of lab_train
y_train = tf.keras.utils.to_categorical(lab_train, num_classes=10)
y_train = tf.constant(y_train, shape=[60000, 10])

## conversion in one-hot vector of lab_test
y_test = tf.keras.utils.to_categorical(lab_test, num_classes=10)
y_test = tf.constant(y_test, shape=[10000, 10])

## check the first 10th element of the one-hot encoding
print(lab_train[0:10])
print(  y_train[0:10])
