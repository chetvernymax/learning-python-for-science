#!/usr/bin/python
# -*- coding: utf-8 -*-
# MIT License, Copyright (c) 2018, Alexandre Boulle

from numpy import *
import matplotlib.pyplot as plt
x = arange(1, 101, 1)
q = arange(-pi/2, pi/2, 0.001)[:,newaxis] # add an axis for further broadcast
f = cos(q*x).sum(axis=1)                  # broadcast q*x, cos and vectorial sum
plt.plot(q,f)
plt.show()
