#!/usr/bin/python
# -*- coding: utf-8 -*-
# MIT License, Copyright (c) 2022, Damien Andre
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import numpy as np
from scipy import ndimage


def rgb2gray(rgb_im):
    r, g, b = rgb_im[:,:,0], rgb_im[:,:,1], rgb_im[:,:,2]
    gray_im = 0.2989 * r + 0.5870 * g + 0.1140 * b
    return gray_im

## read image in grayscale 
img = mpimg.imread('grain.png')
img = rgb2gray(img)

# display image in grayscale for measuring scale
plt.figure()
plt.imshow(img, cmap='gray', vmin=0., vmax=1.)
plt.show()

# pixel to meter conversion
pix2meter = (968-684)/200.

# crop image by removing the info bar at the bottom
img = img[0:680, :]

# display image in grayscale
plt.figure()
plt.imshow(img, cmap='gray', vmin=0., vmax=1.)
plt.show()

# now, we will work on a copy of image
img1 = img.copy()

# apply trehsolding and force the type to be int
treshold = 95/255
img1[img1 < treshold] = 0
img1[img1 >=treshold] = 1
img1 = img1.astype(int)

## display the tresholding image
plt.figure()
plt.imshow(img1, cmap='gray', vmin=0., vmax=1.)
plt.show()

## close the hole
img1 = ndimage.binary_fill_holes(img1)
plt.imshow(img1, cmap='gray', vmin=0., vmax=1.)
plt.show()

