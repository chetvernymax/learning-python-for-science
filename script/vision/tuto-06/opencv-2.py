import numpy as np
import cv2

cap = cv2.VideoCapture(0)

while True:
    ret, frame = cap.read()
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    # get image width and height
    h,w = np.shape(frame)[0:2]
    
    # draw circle
    center_coordinates = (w//2, h//2)
    radius = 20
    color = (0, 255, 0)
    thickness = 5
    frame = cv2.circle(frame, center_coordinates, radius, color, thickness)

    
    # display image
    cv2.imshow('frame', frame)
    if cv2.waitKey(1) == ord('q'):
        break

cap.release()
cv2.destroyAllWindows()
