import numpy as np
import cv2

cap = cv2.VideoCapture(0)


# circle parameters
radius = 20
x , y = (radius, radius)
color = (0, 255, 0)
thickness = 5

# start animation with the keyboard 
start_move = False

while True:
    ret, frame = cap.read()
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    # get image width and height
    h,w = np.shape(frame)[0:2]

    # draw circle
    frame = cv2.circle(frame, (x,y), radius, color, thickness)
    
    # move center of circle
    if x < w and y < h and start_move == True:
        x += 20
        y += 10
    
    # display image
    cv2.imshow('frame', frame)

    # get keyboard input
    key = cv2.waitKey(1)
    if key == ord('q'):   # press 'q' for quit 
        break
    elif key == ord('s'): # press 's' for start/stop the animation
        start_move = not(start_move)

cap.release()
cv2.destroyAllWindows()
