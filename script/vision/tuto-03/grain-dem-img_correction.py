#!/usr/bin/python
# -*- coding: utf-8 -*-
# MIT License, Copyright (c) 2018, Damien Andre

import matplotlib
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import numpy as np

##########
# PART 1 #
##########

## function for color to gray conversion
def rgb2gray(rgb_im):
    r, g, b = rgb_im[:,:,0], rgb_im[:,:,1], rgb_im[:,:,2]
    gray_im = 0.2989 * r + 0.5870 * g + 0.1140 * b
    return gray_im

## read image and convert it
ini_img = mpimg.imread('grain-dem-img.png')
ini_img = rgb2gray(ini_img)

## display original image in grayscale
plt.figure()
plt.imshow(ini_img, cmap='gray', vmin=0., vmax=1.)
plt.show()

## copy initial image
img = ini_img.copy()

## we apply tresholding
img[img<0.5] = 0
img[img>0.5] = 1
plt.imshow(img, cmap='gray', vmin=0., vmax=1.)
plt.show()

## crop image 
grain = img[1020:1180, 1556:1728]
plt.imshow(grain, cmap='gray', vmin=0., vmax=1.)
plt.show()

##########
# PART 2 #
##########
from scipy import ndimage

## get center of grain and display a cross
x,y = np.int_(ndimage.center_of_mass(grain))
grain[x-1:x+2,y      ] = 0.5
grain[x      ,y-1:y+2] = 0.5
plt.imshow(grain, cmap='gray', vmin=0., vmax=1.)
plt.show()

## print the grain center
print("grain center is ({},{}) px".format(x+1020, y+1556))

## get the top left corner of the bounding box 
grain_img = grain
x_array = np.nonzero(np.argmax(grain_img, axis=1))[0]
y_array = np.nonzero(np.argmax(grain_img, axis=0))[0]
xmin = np.min(x_array)
ymin = np.min(y_array)

## get the bottom right corner of the bounding box 
xmax = np.max(x_array)
ymax = np.max(y_array)

## display the corners
grain[xmin, ymin] = 0.5
grain[xmax, ymax] = 0.5
plt.imshow(grain, cmap='gray', vmin=0., vmax=1.)
plt.show()

## display the bounding box
grain[xmin:xmax+1, ymin] = 0.5
grain[xmin:xmax+1, ymax] = 0.5
grain[xmin, ymin:ymax+1] = 0.5
grain[xmax, ymin:ymax+1] = 0.5
plt.imshow(grain, cmap='gray', vmin=0., vmax=1.)
plt.show()

## and print the radius value
diam = ( (xmax-xmin) + (ymax-ymin) ) /2.
print("grain radius is {} px".format(diam/2))


##########
# PART 3 #
##########

## understanding the ndimage.label(...) function
a = np.array([[0,0,1,1,0,0],
              [0,0,0,1,0,0],
              [1,1,0,0,1,0],
              [0,0,0,1,0,0]])

res, N = ndimage.label(a)
print("the labeled array is res=\n", res)
print("the number of label is N=", N)

## now, do this job for all the grains
img       = ini_img.copy() 
img_lb, N = ndimage.label(img)
print("there is {} grains in the image".format(N))

# parse all grain
radius = []
for i in range(1, N+1):
    im = img_lb.copy()
    # apply a filter to display only one grain 
    im[im != i] = 0
    im[im == i] = 1
    # compute grain center and display it
    x,y = np.int_(ndimage.center_of_mass(im))
    img[x-1:x+2,y] = 0.5
    img[x,y-1:y+2] = 0.5
    # compute bounding box 
    x_array = np.nonzero(np.argmax(im, axis=1))[0]
    y_array = np.nonzero(np.argmax(im, axis=0))[0]
    xmax = np.max(x_array)
    xmin = np.min(x_array)
    ymax = np.max(y_array)
    ymin = np.min(y_array)
    # display bounding box
    img[xmin:xmax+1, ymin] = 0.5
    img[xmin:xmax+1, ymax] = 0.5
    img[xmin, ymin:ymax+1] = 0.5
    img[xmax, ymin:ymax+1] = 0.5
    # record the radius in a list
    diam = ( (xmax-xmin) + (ymax-ymin) ) /2.
    radius.append(diam/2)

# display the image
plt.figure()
plt.imshow(img, cmap='gray', vmin=0., vmax=1.)
plt.show()

## print statistical info on grain radii
radius = np.array(radius) # convert the list in numpy array 
print("mean grain radius is {:.2f} px".format(radius.mean()))
print("std  grain radius is {:.2f} px".format(radius.std()))

## an finaly, display histogram
plt.hist(radius, 20, edgecolor = "black")
plt.title("grain radius distribution")
plt.xlabel("grain radius (px)")
plt.ylabel("grain radius occurence")
plt.show()
