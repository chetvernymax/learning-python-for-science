import cv2
from cv2 import aruco
import numpy as np


aruco_dict = aruco.Dictionary_get(aruco.DICT_4X4_50)
parameter  = aruco.DetectorParameters_create()

cap = cv2.VideoCapture(0)

while(True):
    ret, frame = cap.read()
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    corner, num, rejected_point = aruco.detectMarkers(gray, aruco_dict, parameters=parameter)
    frame = aruco.drawDetectedMarkers(frame, corner, num)
    cv2.imshow('frame', frame)

    if cv2.waitKey(1) == ord('q'):   
        break

cap.release()
cv2.destroyAllWindows()
