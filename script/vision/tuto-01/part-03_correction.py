#!/usr/bin/python
# -*- coding: utf-8 -*-
# MIT License, Copyright (c) 2022, Damien Andre

import matplotlib.pyplot as plt
import numpy as np


####################
# Vertical shading #
####################

w = 800 # image width 
h = 600 # image height 
m = np.zeros((h, w)) # build black image 

for i in range(w):
    for j in range(h):
        y = (h - j)/h # normalized x coordinates in the [0, 1[ range 
        m[j][i] = y

plt.imshow(m, cmap='gray', vmin=0., vmax=1.)
plt.show()



##########
# Circle #
##########
w = 800 # image width 
h = 800 # image height 
m = np.zeros((h, w)) # build black image 

for i in range(w):
    for j in range(h):
        x = w/2 - i # centered coordinates in the [-400, +400[ range 
        y = h/2. -j # centered coordinates in the [-400, +400[ range
        n = np.sqrt(x**2 + y**2) # compute the norm
        if n < 200:
            m[j][i] = 1.
        
plt.imshow(m, cmap='gray', vmin=0., vmax=1.)
plt.show()



#################
# Shaded circle #
#################

w = 800 # image width 
h = 800 # image height 
m = np.zeros((h, w)) # build black image 

for i in range(w):
    for j in range(h):
        x = w/2 - i # centered coordinates in the [-400, +400[ range 
        y = h/2. -j # centered coordinates in the [-400, +400[ range
        n = np.sqrt(x**2 + y**2) # compute the norm
        if n < 200:
            m[j][i] = 1.- n/200.
        
plt.imshow(m, cmap='gray', vmin=0., vmax=1.)
plt.show()

