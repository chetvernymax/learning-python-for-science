#!/usr/bin/python
# -*- coding: utf-8 -*-
# MIT License, Copyright (c) 2018, Damien Andre

import numpy as np
from sklearn import datasets
from matplotlib import pyplot as plt
from mlxtend.plotting import plot_decision_regions

# load the data from files
x  = np.fromfile('perceptron-x.dat')
y  = np.fromfile('perceptron-y.dat')
label = np.fromfile('perceptron-label.dat', dtype=int)
data = np.stack((x, y), axis=-1)


# initialize paramaters
w     = np.zeros(2) # it contains (w0,w1)
b     = 0.
eta   = 0.001 # learning rate
epoch = 100   # epoch number
err   = [0]   # to monitor the error

# ... same as before
def sigmoid(x): return 1 / (1 + np.exp(-x))

for i in range(epoch):
    # let's compute, we iterate over all the date 
    for x,t in zip(data,label):
        # feed forward 
        a = np.dot(x, w) + b
        z = sigmoid(a)
        y = z
        # backward propagation
        w += eta*(t-y)*x
        b += eta*(t-y)
        # monitor error
        err.append(err[-1] + np.abs(t-y))

# the perceptron class, this is mandatory for plotting with mlxtend
class Perceptron:
    @staticmethod
    def predict(X): # here, X is a numpy array that contains all the data
        a = np.dot(X, w) + b
        z = sigmoid(a)
        # it returns :
          #  0 if z <= 0.2
          #  1 if z >= 0.8
          # -1 elsewhere
        return np.where(z >= 0.8, 1, (np.where(z<=0.2, 0, -1)))

# plotting
plt.figure()
plot_decision_regions(data, label, clf=Perceptron)

plt.figure()
plt.plot(err)
plt.xlabel("learning step")
plt.ylabel("error")
plt.show()
