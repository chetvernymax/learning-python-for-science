import numpy as np
from sklearn import datasets
from matplotlib import pyplot as plt
from mlxtend.plotting import plot_decision_regions

# make pseudo randomize data 
data,label = datasets.make_blobs(n_samples=1000, centers=2, n_features=2)

# the perceptron class, this is mandatory for plotting with mlxtend
class Perceptron:
    @staticmethod
    def predict(X):
        return np.zeros(len(X))

# plotting
plt.figure()
plot_decision_regions(data, label, clf=Perceptron)
plt.show()
