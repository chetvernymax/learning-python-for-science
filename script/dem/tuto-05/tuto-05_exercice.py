#!/usr/bin/python
# -*- coding: utf-8 -*-
# MIT License, Copyright (c) 2021, Damien Andre


import minidem as dem

# the rigid wall function that uses simple reflection method.
# If a discrete element collide a wall, the velocity of the
# element is modified to simulate an elastic/inelastic collision.
def rigid_wall():
    f = 1. # the elastic/inelastic factor. It must be in the [0;1] range.
    for gr in dem.simu.grain_list:
        
        if gr.pos[0] - gr.radius < 0:
            gr.pos[0] = gr.radius
            if gr.vel[0] < 0.:
                gr.vel[0] *= -f

        elif gr.pos[0] + gr.radius > 100:
            gr.pos[0] = 100 - gr.radius
            if gr.vel[0] > 0.:
                gr.vel[0] *= -f

        if gr.pos[1] - gr.radius < 0:
            gr.pos[1] = gr.radius
            if gr.vel[1] < 0.:
                gr.vel[1] *= -f
            
        elif gr.pos[1] + gr.radius > 100:
            gr.pos[1] = 100 - gr.radius
            if gr.vel[1] > 0.:
                gr.vel[1] *= -f



# here, we add a function for resetting force 
def reset_force():
    for gr in dem.simu.grain_list:
        gr.force = dem.vec(0., 0.)
    
        
# here, we add the contact management from the minidem module
def manage_contact():
    l = dem.lcm.compute_colliding_pair()
    for (gr1,gr2) in l:
        dem.contact(gr1,gr2)


# integration of motion with the velocity verlet algorithm 
def velocity_verlet():
    dt = dem.simu.dt
    for gr in dem.simu.grain_list:
        a = gr.force/gr.mass
        gr.vel += (gr.acc + a) * (dt/2.)
        gr.pos += gr.vel * dt + 0.5*a*(dt**2.)
        gr.acc  = a

# the full time loop
def time_loop():
    reset_force()
    manage_contact()
    velocity_verlet()
    rigid_wall()
    

# the entry point of the program
if __name__ == "__main__":                
    # build domain with one grain at (50,20)
    rad        = 5
    density    = 1
    # first layer of red balls
    grain_1 = dem.grain(dem.vec(50, 20), rad, density) 
    # edit grain's colors
    grain_1.color = "tab:red"
    # set the time step 
    dem.simu.dt = 0.004
    # and run simulation using the time_loop function
    dem.run(tot_iter_number=4000, update_plot_each=10, loop_fn=time_loop)
    print ("End of simulation, the elapsed time is", dem.simu.t, "s")







