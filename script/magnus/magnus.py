#!/usr/bin/python
# -*- coding: utf-8 -*-
# MIT License, Copyright (c) 2018, Damien Andre, Alexandre Boulle


# original video from https://www.youtube.com/watch?v=r-i6XpcL1Fs
# for making image sequence using ffmeg from video
# ffmpeg -i ./magnus-cut.ts -r 24 ./img/output_%04d.png


import glob 
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import numpy as np
import code # for debugging : use 'code.interact(local=locals())' somewhere to get a prompt
import scipy.misc
from scipy import ndimage

# get all image in a list
img_list = sorted(glob.glob('./magnus-img/*.png'))

# to store the trajectory of the ball
position = np.zeros((len(img_list), 2), dtype=int)


## function for gray convertingg 
def rgb2gray(rgb):
    r, g, b = rgb[:,:,0], rgb[:,:,1], rgb[:,:,2]
    gray = 0.2989 * r + 0.5870 * g + 0.1140 * b
    return gray


# parse the list 
for i, f in enumerate(img_list) :
    print ('treat', f)

    # read image 
    img=mpimg.imread(f)
    bw = rgb2gray(img)

    # make a thresholding
    bw = np.where(bw >= .99, 1., 0.)
    bw = bw.astype(int)
    
    
    # now parse the picture and set pixel of zero if the 
    # nearest lth are not white
    bw = scipy.ndimage.morphology.binary_erosion(bw, iterations=10)
    
    # looking for the center of the ball with the mean
    white_index = np.where(bw == 1)

    ############################################################
    ## now remove the white pixel too far the previous center ##
    ############################################################
    max_distance =  50

    if i > 1:
        # get coordinate of the center computed at the last iteration
        x0,y0 = position[i-1][0], position[i-1][1]

        # parse all white pixels
        for idx in range(white_index[0].shape[0]):
            # get coordinate of the current white pixel
            x1,y1 = white_index[0][idx], white_index[1][idx]

            # compute the distance (norm) from center
            norm = ((x1-x0)**2 + (y1-y0)**2)**(0.5)

            # if it is too far change the pixel to black
            if (norm > max_distance):
                bw[x1][y1] = 0
    ####################################################
    ## now compute the center of the white pixel ball ##
    ####################################################
                
    try:
        position[i][0] = int(white_index[0].mean())
        position[i][1] = int(white_index[1].mean())
        # and draw the trajectory on the colored image
        half_spot_size = 5
        for j in range(i):
            x,y = position[j][0], position[j][1]
            for vx in range(-half_spot_size, half_spot_size):
                for vy in range(-half_spot_size, half_spot_size):
                    img[x+vx][y+vy][0] = 1.
    except:
        pass

    # write black and white picture
    new_name = f.replace("./magnus-img", "./modified-img")
    scipy.misc.toimage(bw, cmin=0.0, cmax=1.0).save(new_name)

    # write colored picture
    new_name = f.replace("./magnus-img", "./modified-color-img")
    scipy.misc.toimage(img, cmin=0.0, cmax=1.0).save(new_name)
