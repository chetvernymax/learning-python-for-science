#!/usr/bin/python
# -*- coding: utf-8 -*-
# MIT License, Copyright (c) 2018, Damien Andre

import matplotlib
import matplotlib.pyplot as plt
import numpy as np

font = {'family' : 'normal',
        'weight' : 'bold',
        'size'   : 30}
matplotlib.rc('font', **font)


x = np.linspace(0, 4*np.pi, 100.)
y = np.cos(x) + np.random.randn(len(x))/4
plt.plot(x, y, 'o-', lw=3)


from scipy.optimize import curve_fit

def f(x, a0, a1, a2, a3):
    return a0 + a1 * np.cos(a2 + a3*x)

popt, pcov = curve_fit(f, x, y)
a0, a1, a2, a3 = popt

plt.plot(x, f(x, a0, a1, a2, a3), '--', lw=3)
plt.show()

