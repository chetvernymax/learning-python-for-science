(TeX-add-style-hook
 "ddcommon"
 (lambda ()
   (TeX-add-symbols
    "ddmodulename"
    "ddmodulenumber"
    "ddtitle"
    "ddsubtitle"
    "ddauthor"
    "ddcontributor"
    "dduniv"
    "ddlabs"
    "dddate"
    "ddimg"
    "svgwidth"))
 :plain-tex)

