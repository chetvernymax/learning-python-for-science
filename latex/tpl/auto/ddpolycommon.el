(TeX-add-style-hook
 "ddpolycommon"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("mathpazo" "sc") ("hyperref" "breaklinks=true" "pdfborder={0 0 0}") ("color" "usenames" "dvipsnames") ("geometry" "margin=3cm" "headheight=35pt")))
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "href")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperref")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (TeX-run-style-hooks
    "tpl/ddcommon"
    "mathpazo"
    "hyperref"
    "array"
    "amsmath"
    "marvosym"
    "graphicx"
    "pifont"
    "lastpage"
    "framed"
    "setspace"
    "color"
    "pgfplots"
    "tcolorbox"
    "fancyhdr"
    "geometry"
    "tikz"
    "tocloft")
   (TeX-add-symbols
    '("circled" 1)
    '("correction" 1)
    '("trou" 1)
    '("exo" 1)
    "printddfirstpage"
    "warningsign"
    "resetquestion"
    "ddactivity")
   (LaTeX-add-environments
    "info"
    "attention"
    "manip"
    "question"
    "qnum"
    "task")
   (LaTeX-add-counters
    "question")
   (LaTeX-add-lengths
    "pointwd"
    "mylen"))
 :latex)

