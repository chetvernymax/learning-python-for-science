(TeX-add-style-hook
 "ddpoly-en"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("babel" "english") ("inputenc" "utf8x") ("fontenc" "T1")))
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperref")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "href")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (TeX-run-style-hooks
    "latex2e"
    "tpl/ddpolycommon"
    "article"
    "art10"
    "babel"
    "inputenc"
    "fontenc")
   (TeX-add-symbols
    '("activity" 1)
    "cours"
    "td"
    "tp"
    "projet"
    "ds"
    "note"
    "rien"
    "ddmyversion"
    "ddmypage"
    "mylicence"
    "ddactivity"))
 :latex)

