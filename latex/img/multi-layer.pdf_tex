%% Creator: Inkscape inkscape 0.92.3, www.inkscape.org
%% PDF/EPS/PS + LaTeX output extension by Johan Engelen, 2010
%% Accompanies image file 'multi-layer.pdf' (pdf, eps, ps)
%%
%% To include the image in your LaTeX document, write
%%   \input{<filename>.pdf_tex}
%%  instead of
%%   \includegraphics{<filename>.pdf}
%% To scale the image, write
%%   \def\svgwidth{<desired width>}
%%   \input{<filename>.pdf_tex}
%%  instead of
%%   \includegraphics[width=<desired width>]{<filename>.pdf}
%%
%% Images with a different path to the parent latex file can
%% be accessed with the `import' package (which may need to be
%% installed) using
%%   \usepackage{import}
%% in the preamble, and then including the image with
%%   \import{<path to file>}{<filename>.pdf_tex}
%% Alternatively, one can specify
%%   \graphicspath{{<path to file>/}}
%% 
%% For more information, please see info/svg-inkscape on CTAN:
%%   http://tug.ctan.org/tex-archive/info/svg-inkscape
%%
\begingroup%
  \makeatletter%
  \providecommand\color[2][]{%
    \errmessage{(Inkscape) Color is used for the text in Inkscape, but the package 'color.sty' is not loaded}%
    \renewcommand\color[2][]{}%
  }%
  \providecommand\transparent[1]{%
    \errmessage{(Inkscape) Transparency is used (non-zero) for the text in Inkscape, but the package 'transparent.sty' is not loaded}%
    \renewcommand\transparent[1]{}%
  }%
  \providecommand\rotatebox[2]{#2}%
  \newcommand*\fsize{\dimexpr\f@size pt\relax}%
  \newcommand*\lineheight[1]{\fontsize{\fsize}{#1\fsize}\selectfont}%
  \ifx\svgwidth\undefined%
    \setlength{\unitlength}{250.21044393bp}%
    \ifx\svgscale\undefined%
      \relax%
    \else%
      \setlength{\unitlength}{\unitlength * \real{\svgscale}}%
    \fi%
  \else%
    \setlength{\unitlength}{\svgwidth}%
  \fi%
  \global\let\svgwidth\undefined%
  \global\let\svgscale\undefined%
  \makeatother%
  \begin{picture}(1,0.75446035)%
    \lineheight{1}%
    \setlength\tabcolsep{0pt}%
    \put(0,0){\includegraphics[width=\unitlength,page=1]{multi-layer.pdf}}%
    \put(0.08624874,0.45629948){\color[rgb]{0,0,0}\makebox(0,0)[t]{\lineheight{1.25}\smash{\begin{tabular}[t]{c}$x_1$\end{tabular}}}}%
    \put(0,0){\includegraphics[width=\unitlength,page=2]{multi-layer.pdf}}%
    \put(0.08624874,0.27235395){\color[rgb]{0,0,0}\makebox(0,0)[t]{\lineheight{1.25}\smash{\begin{tabular}[t]{c}$x_2$\end{tabular}}}}%
    \put(0,0){\includegraphics[width=\unitlength,page=3]{multi-layer.pdf}}%
    \put(0.09219154,0.54290589){\color[rgb]{0,0,0}\makebox(0,0)[t]{\lineheight{1.25}\smash{\begin{tabular}[t]{c}input $\mathbf{x}$\end{tabular}}}}%
    \put(0.2390132,0.09252583){\color[rgb]{0,0,0}\makebox(0,0)[t]{\lineheight{1.25}\smash{\begin{tabular}[t]{c}\fbox{weight $\mathbf{w}_{1}$}\end{tabular}}}}%
    \put(0.60149367,0.15376121){\color[rgb]{0,0,0}\makebox(0,0)[t]{\lineheight{1.25}\smash{\begin{tabular}[t]{c}\fbox{weight $\mathbf{w}_{2}$}\end{tabular}}}}%
    \put(0,0){\includegraphics[width=\unitlength,page=4]{multi-layer.pdf}}%
    \put(0.08916238,0.74231437){\color[rgb]{0,0,0}\makebox(0,0)[t]{\lineheight{1.25}\smash{\begin{tabular}[t]{c}input layer ($l=0$)\end{tabular}}}}%
    \put(0,0){\includegraphics[width=\unitlength,page=5]{multi-layer.pdf}}%
    \put(0.41761578,0.74231437){\color[rgb]{0,0,0}\makebox(0,0)[t]{\lineheight{1.25}\smash{\begin{tabular}[t]{c}hidden layer ($l=1$)\end{tabular}}}}%
    \put(0,0){\includegraphics[width=\unitlength,page=6]{multi-layer.pdf}}%
    \put(0.70299604,0.74231437){\color[rgb]{0,0,0}\makebox(0,0)[t]{\lineheight{1.25}\smash{\begin{tabular}[t]{c}output layer ($l=2$)\end{tabular}}}}%
    \put(0.83032248,0.37908107){\color[rgb]{0,0,0}\makebox(0,0)[lt]{\lineheight{1.25}\smash{\begin{tabular}[t]{l}output $\mathbf{y = z_2}$\end{tabular}}}}%
    \put(0.27926948,0.59011761){\color[rgb]{0,0,0}\rotatebox{27.37598}{\makebox(0,0)[t]{\lineheight{1.25}\smash{\begin{tabular}[t]{c}$w_{1_{11}}$\end{tabular}}}}}%
    \put(0.30739321,0.53016732){\color[rgb]{0,0,0}\rotatebox{52.437718}{\makebox(0,0)[t]{\lineheight{1.25}\smash{\begin{tabular}[t]{c}$w_{1_{12}}$\end{tabular}}}}}%
    \put(0.30204988,0.40993629){\color[rgb]{0,0,0}\rotatebox{-26.929323}{\makebox(0,0)[t]{\lineheight{1.25}\smash{\begin{tabular}[t]{c}$w_{1_{21}}$\end{tabular}}}}}%
    \put(0.29217827,0.35606353){\color[rgb]{0,0,0}\rotatebox{23.339466}{\makebox(0,0)[t]{\lineheight{1.25}\smash{\begin{tabular}[t]{c}$w_{1_{22}}$\end{tabular}}}}}%
    \put(0.32064996,0.22231062){\color[rgb]{0,0,0}\rotatebox{-55.890071}{\makebox(0,0)[t]{\lineheight{1.25}\smash{\begin{tabular}[t]{c}$w_{1_{31}}$\end{tabular}}}}}%
    \put(0.28587075,0.17391459){\color[rgb]{0,0,0}\rotatebox{-30.701419}{\makebox(0,0)[t]{\lineheight{1.25}\smash{\begin{tabular}[t]{c}$w_{1_{32}}$\end{tabular}}}}}%
    \put(0.58054107,0.25839184){\color[rgb]{0,0,0}\rotatebox{40.141402}{\makebox(0,0)[t]{\lineheight{1.25}\smash{\begin{tabular}[t]{c}$w_{13}$\end{tabular}}}}}%
    \put(0.56009822,0.37679592){\color[rgb]{0,0,0}\rotatebox{-0.3363126}{\makebox(0,0)[t]{\lineheight{1.25}\smash{\begin{tabular}[t]{c}$w_{12}$\end{tabular}}}}}%
    \put(0.59194833,0.49577981){\color[rgb]{0,0,0}\rotatebox{-42.194284}{\makebox(0,0)[t]{\lineheight{1.25}\smash{\begin{tabular}[t]{c}$w_{11}$\end{tabular}}}}}%
    \put(0.72359805,0.30968236){\color[rgb]{0,0,0}\makebox(0,0)[t]{\lineheight{1.25}\smash{\begin{tabular}[t]{c}$z_2$\end{tabular}}}}%
    \put(0.67655474,0.30968236){\color[rgb]{0,0,0}\makebox(0,0)[t]{\lineheight{1.25}\smash{\begin{tabular}[t]{c}$a_2$\end{tabular}}}}%
    \put(0.4400979,0.30769594){\color[rgb]{0,0,0}\makebox(0,0)[t]{\lineheight{1.25}\smash{\begin{tabular}[t]{c}$z_{1_{2}}$\end{tabular}}}}%
    \put(0.39305468,0.30769594){\color[rgb]{0,0,0}\makebox(0,0)[t]{\lineheight{1.25}\smash{\begin{tabular}[t]{c}$a_{1_{2}}$\end{tabular}}}}%
    \put(0.4400979,0.57211618){\color[rgb]{0,0,0}\makebox(0,0)[t]{\lineheight{1.25}\smash{\begin{tabular}[t]{c}$z_{1_{1}}$\end{tabular}}}}%
    \put(0.39904955,0.57211618){\color[rgb]{0,0,0}\makebox(0,0)[t]{\lineheight{1.25}\smash{\begin{tabular}[t]{c}$a_{1_{1}}$\end{tabular}}}}%
    \put(0.4400979,0.04648723){\color[rgb]{0,0,0}\makebox(0,0)[t]{\lineheight{1.25}\smash{\begin{tabular}[t]{c}$z_{1_{3}}$\end{tabular}}}}%
    \put(0.39305468,0.04648723){\color[rgb]{0,0,0}\makebox(0,0)[t]{\lineheight{1.25}\smash{\begin{tabular}[t]{c}$a_{1_{3}}$\end{tabular}}}}%
  \end{picture}%
\endgroup%
