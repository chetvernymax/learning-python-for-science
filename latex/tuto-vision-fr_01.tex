\documentclass[a4paper, trou]{tpl/ddpoly-en}
\usepackage{dirtree}
\usepackage{wasysym}
\usepackage{mathtools}
\usepackage{enumitem}
\setlist{nolistsep}

\input{macro}
\input{tpl/listing-config}

  
\graphicspath{{tpl/}{img/}}
\ddmodulename{Informatique}
\ddmodulenumber{Vision par ordinateur}
\ddauthor{D. André}
\dduniv{Ensil-Ensci}
\ddimg{\begin{center}\includegraphics[width=6cm]{img/arduino-logo}\end{center}}
\rien




\begin{document}
\renewcommand{\labelitemi}{$\bullet$}
\lstset{language=granCpp,style=granCpp}  
\lstset{language=granPython,style=granPython}  

\newvisionexo{01}{Comprendre le format d'une image}

\noindent L'objectif de ce tuto est de comprendre la structure
informatique d'une image. Attention, ici nous verrons la structure la
plus simple que l'on appelle communément \textit{image matricielle} ou
\textit{bitmap} (en anglais) où une image est simplement représentée
par un tableau bi-dimensionnel (matrice). Chaque élément de ce tableau
représente la couleur (ou le niveau de gris) d'un pixel. De cette
façon, il est possible de reconstruire, pixel par pixel une image
complexe (comme une photographie par exemple). La figure ci-dessous
illustre ce format de données pour une image de $3\times 3$ pixels en niveau
de gris. Ici, une valeur comprise dans l'intervalle [0,1] est associée
à chaque pixel (\texttt{0} pour noir, \texttt{0.x} pour gris et \texttt{1} pour blanc).
\begin{center}\includegraphics[width=.7\linewidth]{./img/img-matrix}\end{center}

\subsection*{Partie 1 : construction à la main d'une image en niveau de gris }

L'image précédante a été obtenue à l'aide du script suivant.
\vspace*{-1.5\baselineskip}
\begin{lstlisting}
import matplotlib.pyplot as plt

m = [[0.1, 0.2, 0.3],
     [0.4, 0.5, 0.6],
     [0.7, 0.8, 0.9]]

plt.imshow(m, cmap='gray', vmin=0., vmax=1.)
plt.show()
\end{lstlisting}

\begin{manip}
  Recopiez ce script et exécutez-le
\end{manip}

\begin{task}
  Modifiez ce script de façon à ce que le pixel central soit blanc et tous les autres noirs.
\end{task}

\begin{task}
  Réalisez le même motif, mais pour une image de $9\times 9$ pixels.
\end{task}

\begin{task}
  Faites maintenant en sorte que les pixels "qui touchent" le pixel
  blanc deviennent gris. Vous devriez alors obtenir une image telle
  que celle-ci : \dltutovision{01}{part-01\_correction.svg}.
\end{task}

\begin{info}
  La correction de cette partie est donnée par \dltutovision{01}{part-01\_correction.py}
\end{info}


\subsection*{Partie 2 : construction automatisée d'une image en niveau de gris }

Il n'est pas raisonnable de construire "à la main" des matrices
d'image pour des tailles supérieures à $10\times 10$ pixels. Dans cette
partie, nous allons donc voir comment automatiser ce processus de
construction.

\begin{task}
  Construisez une image noire de $800\times 600$ (800 px de large et 600 px de haut). Vous pouvez
  utiliser deux boucles imbriquées ou bien toute autre méthode que vous jugerez pertinente.
\end{task}

Pour la suite, nous utiliserons la bibliothèque \texttt{numpy}. Cette
bibliothèque est un outil incontournable pour le traitement numérique
en Python. Une des briques de base de \texttt{numpy} est les tableaux
de données \href{https://numpy.org/devdocs/user/basics.creation.html}{\texttt{np.array}}. Nous allons maintenant utiliser les
tableaux \texttt{numpy} comme conteneur de nos images plutôt que des listes imbriquées. 

\begin{manip}
  Importer la bibliothèque numpy à l'aide du code suivant :
  \vspace*{-1.5\baselineskip}
\begin{lstlisting}
import numpy as np
\end{lstlisting}
\end{manip}

\begin{task}
  À l'aide de la fonction
  \href{https://numpy.org/doc/stable/reference/generated/numpy.zeros.html}{\texttt{np.zeros(...)}}
  construisez une image noire de $800\times 600$.
\end{task}

\begin{task}
  À l'aide de la fonction
  \href{https://numpy.org/doc/stable/reference/random/generated/numpy.random.rand.html}{\texttt{np.random.rand(...)}}
  construisez une image de $800\times 600$ dont les pixels ont des
  niveaux de gris aléatoires.
\end{task}

\begin{info}
  La correction de cette partie est donnée par \dltutovision{01}{part-02\_correction.py}
\end{info}

\subsection*{Partie 3 : les dégradés}
\begin{manip}
  Réécrivez et exécutez le script suivant
  \vspace*{-1.5\baselineskip}
\begin{lstlisting}
import matplotlib.pyplot as plt
import numpy as np

w = 800 # image width
h = 600 # image height
m = np.zeros((h, w)) # build black image

for i in range(w):
    for j in range(h):
        x = (w - i)/w # normalized x coordinates in the [0, 1[ range
        m[j][i] = x

plt.imshow(m, cmap='gray')
plt.show()
\end{lstlisting}
\end{manip}

\begin{task}
  En vous inspirant du script précédant, réalisez un dégradé
  vertical. Vous devriez alors obtenir une image telle
  que celle-ci : \dltutovision{01}{part-03\_correction\_vertical-shading.svg}.
\end{task}

\begin{task}
  Changez la taille de l'image en $800\times 800$ puis, en vous
  inspirant du script précédant, dessinez un cercle blanc de rayon de
  200px au centre de l'image. Vous devriez alors obtenir une image
  telle que celle-ci :
  \dltutovision{01}{part-03\_correction\_circle.svg}.
\end{task}

\begin{task}
  Réalisez alors un dégradé circulaire à l'intérieur de ce cercle de
  façon obtenir une image telle que celle-ci :
  \dltutovision{01}{part-03\_correction\_circle-shaded.svg}.
\end{task}

\begin{info}
  La correction de cette partie est donnée par \dltutovision{01}{part-03\_correction.py}
\end{info}

\subsection*{Partie 4 : un peu d'algèbre linéaire avec \texttt{numpy}}
Dans cette partie, nous allons utiliser des fonctions avancées de la
bibliothèque \texttt{numpy}. Le script suivant tire parti des
capacités de \texttt{numpy} à réaliser des calculs d'algèbre
linéaire. Pour ce faire, nous réaliserons des produits scalaires entre des matrices
colonnes et des matrices lignes de façon à obtenir des matrices pleines telles
que le montre l'exemple ci-dessous :
\begin{align}\underbrace{
  \begin{bmatrix}
    c_1 \\ c_2        
  \end{bmatrix}}_{\text{matrice colonne }(1\times 2)}\bullet
  \underbrace{\begin{bmatrix}
    l_1 & l_2 & l_3
  \end{bmatrix}}_{\text{matrice ligne }(3\times 1)} = 
  \underbrace{\begin{bmatrix}
    l_1 \cdot c_1 &  l_2 \cdot c_1 & l_3 \cdot c_1 \\
    l_1 \cdot c_2 &  l_2 \cdot c_2 & l_3 \cdot c_2
  \end{bmatrix}}_{\text{matrice pleine }(3\times 2)}
  \nonumber
\end{align}

\begin{manip}
  Réécrivez et exécutez le script suivant
  \vspace*{-1.5\baselineskip}
\begin{lstlisting}
import matplotlib.pyplot as plt
import numpy as np

x = np.linspace(0., 1., 100) # create 1D array [0.00, 0.01, 0.02, ..., 1.00]
y = np.ones(100)             # create 1D array [1.00, 1.00, 1.00, ..., 1.00]

x = x[np.newaxis,:] # build a line matrix
y = y[:,np.newaxis] # build a column matrix
m = np.dot(y, x)    # build a matrix thanks to the dot product

plt.imshow(m, cmap='gray', vmin=0., vmax=1.)
plt.show()
\end{lstlisting}
\end{manip}

\begin{task}
  En vous inspirant du script précédant, réalisez un dégradé
  vertical. Vous devriez alors obtenir une image telle
  que celle-ci : \dltutovision{01}{part-04\_correction\_vertical-shading.svg}.
\end{task}

\begin{task}
  En vous inspirant du script précédant et en prenant \texttt{x} et \texttt{y} dans l'intervalle $[0, \pi[$,
  exploitez les fonctions trigonométriques de \texttt{numpy} de façon à créer un dégradé circulaire tel que 
  le montre cette figure : \dltutovision{01}{part-04\_correction\_spherical-shading.svg}.
\end{task}

\begin{task}
  En vous inspirant du script précédant et à l'aide de la fonction
  \href{https://numpy.org/doc/stable/reference/generated/numpy.tile.html}{\texttt{np.tile(...)}},
  répétez dix fois le motif précédant dans les deux directions (cela
  fait $10\times 10 = 100$ répétitions). L'objectif est d'obtenir
  l'image suivante :
  \dltutovision{01}{part-04\_correction\_spherical-shadingx10.svg}.
  
\end{task}

\begin{info}
  La correction de cette partie est donnée par \dltutovision{01}{part-04\_correction.py}
\end{info}

\end{document}
