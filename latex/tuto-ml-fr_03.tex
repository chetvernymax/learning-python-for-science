\documentclass[a4paper, trou]{tpl/ddpoly-en}
\usepackage{dirtree}
\usepackage{wasysym}
\usepackage{mathtools}
\usepackage{enumitem}
\setlist{nolistsep}

\input{macro}
\input{tpl/listing-config}

  
\graphicspath{{tpl/}{img/}}
\ddmodulename{Informatique}
\ddmodulenumber{Machine learning}
\ddauthor{D. André}
\dduniv{Ensil-Ensci}
\ddimg{\begin{center}\includegraphics[width=6cm]{img/arduino-logo}\end{center}}
\activity{\textbf{Crédit} : \href{https://github.com/cjlux/APP-ML/blob/master/APP-ML\_2023/Notebooks}{JL. Charles} (CC BY-SA 4.0)}




\begin{document}
\renewcommand{\labelitemi}{$\bullet$}
\lstset{language=granCpp,style=granCpp}  
\lstset{language=granPython,style=granPython}  

\newmlexo{03}{préparer ses données}

\noindent L'objectif de ce tuto est de préparer les données de la
banque \textsc{mnist} en vue d'alimenter un réseau dense de
neurones. En effet, un réseau de neurones a besoin de travailler sur
des données \textbf{normalisées} (comprises entre 0 et 1) et sur des
données \textbf{vectorisées} (tableau de données à une seule
dimension). Il est donc nécessaire de ré-encoder les données pour
satisfaire ces deux critères.

L'image suivante montre le réseau de neurones qui sera implémenté dans
le tuto suivant. Comme le montre cette image, il devra être présenté
en entrée du réseau de neurones un vecteur normalisé contenant 784
valeurs (correspondant aux images) tandis que la sortie sera un
vecteur normalisé de 10 valeurs (correspondant aux labels).

\begin{center}
  \includegraphics[width=\linewidth]{archi-dnn-mnist}
\end{center}


\begin{manip}
  Créez un environnement Python et importez la banque de données du
  \textsc{mnist} dans les variables suivantes : \texttt{im\_train},
  \texttt{lab\_train}, \texttt{im\_test} et \texttt{lab\_test} (voir
  tuto précédant).
\end{manip}

\subsection*{Partie 1 : préparation des images}

\begin{info}
Les images du \textsc{mnist} sont encodées par des nombres entiers compris entre 0 et 255.
\end{info}

\begin{task}
  Normalisez les images de façon à les encoder par des flottants compris entre 0 et 1.
\end{task}

\begin{info}
  Nous souhaitons maintenant vectoriser les images, c'est à dire, passer d'une image encodée 
  par une matrice en deux dimensions $n \times m$ à un vecteur de données à une seule dimension.
\end{info}

\begin{task}
  Sachant que les images sont encodées par des matrices de
  $28 \times 28$, quelle doit être la taille du vecteur associé à l'image ? 
\end{task}

\begin{task}
  À l'aide de la fonction
  \href{https://www.w3resource.com/numpy/manipulation/reshape.php}{\texttt{reshape(...)}}
  qui permet de changer les dimensions d'un tableau \keyw{numpy}, vectorisez
  les tableaux \keyw{im\_train} et \keyw{im\_test}. Nommez ces 2
  nouveaux tableaux respectivement \keyw{x\_train} et \keyw{x\_test}
\end{task}


\begin{info}
  La correction de cette partie est donnée par \dltutoml{03}{part-1.py}
\end{info}

\subsection*{Partie 2 : préparation des labels}
\begin{info}
  Les labels (\keyw{lab\_train} et \keyw{lab\_test}) donnent
  directement les valeurs des chiffres correspondants aux images. Par
  exemple, la 108ème valeur de \keyw{lab\_train} est le chiffre
  '\keyw{0}'. Or, ce type d'encodage n'est pas compatible avec les
  réseaux de neurones. Nous allons avoir besoin d'un encodage dit 
  \href{https://fr.wikipedia.org/wiki/Encodage_one-hot}{\textbf{one-hot}}.
\end{info}

Dans notre cas, nous ne pouvons avoir que 10 valeurs possibles (0, 1,
2, 3, 4, 5, 6, 7, 8, 9). Le vecteur one-hot aura alors une taille de
10. La correspondance chiffres $\leftrightarrow$ vecteur one-hot est
alors donnée par le tableau suivant.
\begin{center}
\begin{tabular}{ccc}
  \textit{chiffre} &   & \textit{vecteur one-hot }\\\hline
  \texttt{0}       & $\rightarrow$ &\texttt{[1, 0, 0, 0, 0, 0, 0, 0, 0, 0]}\\
  \texttt{1}       & $\rightarrow$ &\texttt{[0, 1, 0, 0, 0, 0, 0, 0, 0, 0]}\\
  \texttt{2}       & $\rightarrow$ &\texttt{[0, 0, 1, 0, 0, 0, 0, 0, 0, 0]}\\
  \texttt{3}       & $\rightarrow$ &\texttt{[0, 0, 0, 1, 0, 0, 0, 0, 0, 0]}\\
  $\vdots$         & $\vdots$      & $\vdots$ \\
  \texttt{9}       & $\rightarrow$ &\texttt{[0, 0, 0, 0, 0, 0, 0, 0, 0, 1]}\\            
\end{tabular}
\end{center}

\begin{task}
  Consultez la documentation de
  \href{https://www.tensorflow.org/api_docs/python/tf/keras/utils/to_categorical}{\texttt{tf.keras.utils.to\_categorical}}
  et en déduire comment convertir \keyw{lab\_train} et
  \keyw{lab\_test} en tableaux one-hots. Nommez ces deux nouveaux
  tableaux \keyw{y\_train} et \keyw{y\_test}.
\end{task}

\begin{task}
  Visualiser les 10 premières valeurs des tableaux \keyw{lab\_train}
  et \keyw{y\_train} de façon à vérifier que l'encodage one-hot
  s'est bien déroulé.
\end{task}

\begin{info}
  La correction de cette partie est donnée par \dltutoml{03}{part-2.py}
\end{info}

\begin{info}
  Félicitation, vos données sont maintenant prêtes à être présentées à un réseau de neurones !
\end{info}

\subsection*{Partie 3 : défi ML!}

\begin{task}
  À partir du tableau \keyw{x\_train}, créez un nouveau tableau qui,
  pour chaque image, contient uniquement \textbf{la moyenne} des
  niveaux des gris de chacune des images. Nommez ce tableau \keyw{x\_train\_mean}.
\end{task}

\begin{info}
  Si vous affichez le tableau \keyw{x\_train\_mean}, vous devriez obtenir :
  \vspace*{-1.5\baselineskip}
\begin{lstlisting}
>>> print(x_train_mean)
[0.13768007 0.15553721 0.0972539  ... 0.11070428 0.10218087 0.10464186]
\end{lstlisting}
\end{info}


\begin{task}
  Tracez l'histogramme de la répartition des moyennes uniquement pour le chiffre
  0. Vous devriez obtenir l'image suivante \dltutoml{03}{challenge-hist-1.svg}
\end{task}

\begin{task}
  Tracez un histogramme 3D de la répartition des moyennes pour tous
  les chiffres. Vous devriez obtenir l'image suivante
  \dltutoml{03}{challenge-hist-2.svg}
\end{task}

\end{document}
